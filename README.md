# GitLab CI Catalog, Components and Templates

**GitLab CI CCT** is a comprehensive **GitLab** CI/CD YAML template repository.


## GitLab CI Notes, a Pros/Cons approach

The following notes are factual and present the different GitLab CI modes in a pros & cons approach.

### Remote includes

* Preview of variables in `run pipeline` action
* Independent of instance
* No customization of pipeline, restricted to gitlab-ci features
* YAML CI files must be accessible via public https
* Additional and nested includes are not relative to initial include
* Sample usage: `- remote: "${CI_SERVER_URL}/${CI_PROJECT_PATH}/-/raw/${CI_COMMIT_REF_NAME}/templates/core/template.yml"`
* Usage of always up to date version (main, master, latest), or of a reference

### Component catalog files

* Preview of variables in `run pipeline` action
* Can not be used across instances (as of GitLab 16.4)
* No customization of pipeline, restricted to gitlab-ci features
* Template Catalogue project may be an internal or private project
* Additional and nested includes are not relative to initial include
* Sample usage: `- component: "${CI_SERVER_HOST}/${CI_PROJECT_PATH}/templates/core@${CI_COMMIT_REF_NAME}"`
* Usage of always up to date version (main, master, latest), or of a reference

### Generated triggered pipelines

* No preview of variables in `run pipeline` action
* Independent of instance
* Customization of pipeline possible, and extension of available gitlab-ci features
* Initial YAML CI file may be local, remote, or component include
* Additional and nested includes can be local and passed as artifact.
* Sample usage: n/a
* Usage of always up to date version (main, master, latest), or of a reference

### Local includes
* Preview of variables in `run pipeline` action
* Independent of instance
* No customization of pipeline, restricted to gitlab-ci features
* Initial YAML CI file is local
* Additional and nested includes are local and not relative to initial file
* Sample usage: `- local: "templates/core"`
* Usage of current version only

### Ideal system
* Preview of variables in `run pipeline` action
* Can be used across instances
* Customization of pipeline possible, and extension of available gitlab-ci features
* Initial YAML CI file may be local, remote, or component include
* Additional and nested includes are relative to current include
* Usage of always up to date version (main, master, latest), or of a reference

### Misc.

* When we split file into several, then when branching to test, we need to change
the reference (ie: master) and risk forgetting to set it back.

* Components are useful if it will be used several times with differents values.

* Otherwise, components have a nicer inclusion syntax.

* Globally, there are different "steps" in creating a CI pipeline:
  * Stage declaration
  * Workflow declaration
  * Defauts declarations
  * Jobs
  * Speciality jobs (pages, releases)

* When using variables in inclusion statements, the variables will not be shown to user.


## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.


## Locations

  * Documentation: [https://gitlab-ci.docs.twiddle-z.com/](https://gitlab-ci.docs.twiddle.com/)
  * Website: [https://gitlab.com/twiddle-z/gitlab-ci/](https://gitlab.com/twiddle-z/gitlab-ci/)
