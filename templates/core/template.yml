# ==============================================================================
# Core
# ==============================================================================
spec:
  inputs:
    CI_DISABLE_JOBS:
      default: ""
    CI_ENABLE_JOBS:
      default: ""

---
variables:
  CI_DISABLE_JOBS:
    value: "$[[ inputs.CI_DISABLE_JOBS ]]"
    description: "Space delimited list of jobs to disable."
  CI_ENABLE_JOBS:
    value: "$[[ inputs.CI_ENABLE_JOBS ]]"
    description: "Space delimited list of jobs to force enable."

# The following section add functions to the bash executor of the image.
#   * we disable PS4 (used by `set -x`)
#   * we create functions to create collapsible timed sections
#   * we create some coloring functions
#
# Collapsible sections:
#   https://docs.gitlab.com/ee/ci/jobs/#custom-collapsible-sections
#
# Usage:
#   - !reference [".core:functions", script]
.core:functions:
  script:
    - |
      # !reference [".core:functions", script]
      export PS4=''
      section_start()    { echo -e "section_start:`date +%s`:${1}${3}\r\e[0K\e[34;1m${2}\e[0m"; }
      section_start_c()  { section_start "$1" "$2" "[collapsed=true]"; }
      section_start_nc() { section_start "$1" "$2" "[collapsed=false]"; }
      section_end()      { echo -e "section_end:`date +%s`:${1}\r\e[0K"; }
      # Title is bluish like section starts
      color_title()      { echo -e "\e[1;34m"${@}"\e[0m"; }
      color_error()      { echo -e "\e[1;31m"${@}"\e[0m"; }
      color_warning()    { echo -e "\e[1;33m"${@}"\e[0m"; }


# ------------------------------------------------------------------------------
# .core:prolog
# ------------------------------------------------------------------------------
# Print debugging info, notably details that allow
# to retrieve instance, project, pipeline and job
# from the raw log files on server.
#
.core:prolog:
  script:
    - |
      # !reference [".core:prolog", script]
      section_start_c "core_prolog" "Core debugging information"
      cat <<_EOF
      Current:
        time:     $(date -R)
        server:   ${CI_SERVER_URL}
        project:  ${CI_PROJECT_URL}
        pipeline: ${CI_PIPELINE_URL}
        job:      ${CI_JOB_URL}

        source:    ${CI_PIPELINE_SOURCE}
        protected: ${CI_COMMIT_REF_PROTECTED}

        server version: ${CI_SERVER_VERSION}
        runner version: ${CI_RUNNER_VERSION}
        runner id:      ${CI_RUNNER_ID}
      _EOF
      section_end "core_prolog"


# ------------------------------------------------------------------------------
# .core:defaults
# ------------------------------------------------------------------------------
# The following contains keywords that are not
# allowed in a `default` section.
#
# As a CI catalog, we don't touch `defaults` at all.
#
# cf. https://docs.gitlab.com/ee/ci/yaml/#default
.core:defaults:
  needs: []
  cache: null
  before_script:
    - !reference [".core:functions", script]
    - !reference [".core:prolog",    script]
  script:
    - |
      #
      /bin/true
  rules:
    - when: never
  allow_failure: false


# ------------------------------------------------------------------------------
# .core:none
# ------------------------------------------------------------------------------
# Disable everything that could be inherited
.core:none:
  extends:
    - .core:defaults
  image: alpine
  services: []
  variables:
    GIT_STRATEGY: none
  inherit:
    default: false
    variables: false

# ------------------------------------------------------------------------------
# .core:dummy
# ------------------------------------------------------------------------------
# cf: https://docs.gitlab.com/ee/ci/yaml/#inheritdefault
.core:dummy:
  extends:
    - .core:none
  stage: build
  script:
    - |
      # no-op
      /bin/true
  after_script:
    - |
      # no-op
      /bin/true
  rules:
    - when: always
