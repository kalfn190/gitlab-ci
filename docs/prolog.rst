.. Environment variables

.. role:: ev
  :class: envvar

.. Word emphasis

.. role:: em
  :class: em-emphasis

.. Highlighted word emphasis

.. role:: hi
  :class: hi-emphasis

.. An uncheck checkmark

.. |uncheck| raw:: html

  <input type="checkbox">
