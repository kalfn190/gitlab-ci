Misc
====

.. include:: shared/local-toc.rst

Usage
-----

.. templates/misc/template.yml

.. code-block:: yaml

  include:
    - component: "${CI_CATALOG_PATH}/templates/misc@${CI_CATALOG_REF}"


Variables
---------

None.


.misc:user:pre
----------------

Defines blank ``before_script`` and ``after_script`` that can be overwritten by user. This sections will be called by all **misc** jobs.

Usage
^^^^^

.. code-block:: yaml

  .misc:user:pre:
    before_script:
      - echo "Hello world"


.misc:user:post
-----------------

Defines blank ``before_script`` and ``after_script`` that can be overwritten by user. This sections will be called by all **misc** jobs.

Usage
^^^^^

.. code-block:: yaml

  .misc:user:post:
    before_script:
      - echo "Hello world"

.misc:defaults
----------------

Defaults for any **misc** job.
