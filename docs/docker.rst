Docker
======

.. include:: shared/local-toc.rst

Usage
-----

.. templates/docker/template.yml

.. code-block:: yaml

  include:
    - component: "${CI_CATALOG_PATH}/templates/docker@${CI_CATALOG_REF}"


Variables
---------

The following environment variables can be set via **catalog specs**:

* :ev:`DOCKER_DIND`: `Docker-in-Docker`_ image to use as a service.
* :ev:`DOCKER_HOST`: Specifies the address of the Docker daemon.
* :ev:`DOCKER_TLS_CERTDIR`: Docker-in-Docker TLS path.
* :ev:`DOCKER_JOB_IMAGE`: Docker image to use for build environment. Requires a ``docker`` executable to be present.
* :ev:`DOCKER_FILE_DOCKER_CONFIG`: **String** or **file variable** that contains the contents or file location of the :file:`~/.docker/config`.

.. _Docker-in-Docker: https://hub.docker.com/_/docker


.docker:user:pre
----------------

Defines blank ``before_script`` and ``after_script`` that can be overwritten by user. This sections will be called by all **docker** jobs.

Usage
^^^^^

.. code-block:: yaml

  .docker:user:pre:
    before_script:
      - echo "Hello world"


.docker:user:post
-----------------

Defines blank ``before_script`` and ``after_script`` that can be overwritten by user. This sections will be called by all **docker** jobs.

Usage
^^^^^

.. code-block:: yaml

  .docker:user:post:
    before_script:
      - echo "Hello world"


.docker:defaults
----------------

Defaults for any **docker** job, it includes a **docker:dind** service, copies configuration and runs ``docker login``.


.docker:config
--------------

Contains a ``script`` section that will copy contents or file from :ev:`DOCKER_FILE_DOCKER_CONFIG` to it's :file:`~/.docker/config` path.


.docker:login
-------------

The ``script`` will authenticate against the Gitlab registry if the later is activated. This is done after ``.docker:config`` so that other logins or settings aren't overwritten.
