Core
====

.. include:: shared/local-toc.rst

Usage
-----

.. templates/core/template.yml

.. code-block:: yaml

  include:
    - component: "${CI_CATALOG_PATH}/templates/core@${CI_CATALOG_REF}"


Variables
---------

The following environment variables can also be passed as **catalog spec inputs**:

* :ev:`CI_ENABLE_JOBS`: Space delimited list of jobs to force enable.
* :ev:`CI_DISABLE_JOBS`: Space delimited list of jobs to disable.

Usage
^^^^^

.. code-block:: yaml

  variables:
    CI_ENABLE_JOBS: "misc:precommit"
    CI_DISABLE_JOBS: "python:black"


.core:functions
---------------

The ``script`` section defines the following shell functions:

* :em:`section_start`: Start a collapsible section.
* :em:`section_start_c`: Start a collapsible section in a collapsed state.
* :em:`section_start_nc`: Start a collapsible section in a non collapsed state.
* :em:`section_end`: End of a collapsible section.
* :em:`color_title`: Print a title in blue.
* :em:`color_error`: Print an error in red.
* :em:`color_warning`: Print a warning in yellow.

Usage
^^^^^

.. code-block:: yaml

  example:
    stage: build
    script:
      - !reference [".core:functions", script]

      - section_start_c "example" "This is an example title"
      - /bin/true
      - section_end "example"


.core:prolog
------------

This ``script`` prints core debugging information. While some information seems obvious in the UI, the values may not be present by default in the job log. If given only the job logs, the obvious values will provide the information to find the project, pipeline and job.

Usage
^^^^^

.. code-block:: yaml

  example:
    stage: build
    script:
      - !reference [".core:prolog", script]


.core:defaults
--------------

Job that defines defaults for all jobs. The ``before_script`` section can also be used independently.

Usage
^^^^^

.. code-block:: yaml

  example:
    extends:
      - .core:defaults

.. code-block:: yaml

  example:
    script:
      - !reference [".core:defaults", before_script]


.core:none
----------

Job that disables everything that could be inherited.

Usage
^^^^^

.. code-block:: yaml

  example:
    extends:
      - .core:none


.core:dummy
-----------

A dummy job that can be enabled by extending it in order for pipelines to always have a minimum of one job.


Usage
^^^^^

.. code-block:: yaml

  dummy:
    extends:
      - .core:dummy
