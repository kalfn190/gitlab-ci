Workflows
=========

.. include:: shared/local-toc.rst

Usage
-----

.. templates/workflows/template.yml

.. code-block:: yaml

  include:
    - component: "${CI_CATALOG_PATH}/templates/workflows@${CI_CATALOG_REF}"


Default workflow
----------------

Contrary to the GitLab defaults, the GitLab CI CCT project uses a workflow with no duplicate pipelines.


Variables
---------

* :ev:`PIPELINE_NAME`: Internal. Defaults to "Unnamed pipeline" and is used to give the pipeline a name.
* :ev:`CI_IS_MERGE_REQUEST`: Internal. Set to true if pipeline is for a merge request.
* :ev:`CI_IS_TAG`: Internal. Set to true if pipeline is for a tag.
* :ev:`CI_IS_BRANCH`: Internal. Set to true if pipeline is for a branch.
