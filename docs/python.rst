Python
======

.. include:: shared/local-toc.rst

Usage
-----

.. templates/python/template.yml

.. code-block:: yaml

  include:
    - component: "${CI_CATALOG_PATH}/templates/python@${CI_CATALOG_REF}"


Variables
---------

The following environment variables can be set via **catalog specs**:

The following environment variables can not be set via **catalog specs**.

.. * PYTHON_FILE_PIP_CONF
.. * PYTHON_FILE_PYPIRC
