Caveats
=======

* Using a variable in a catalog inclusion prevents showing the variables in the UI (`#430786`_).

.. _#430786: https://gitlab.com/gitlab-org/gitlab/-/issues/430786
